#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>

#define I2C_SDA 33
#define I2C_SCL 32
#define PIN_1WIRE 33
#define SENSOR_POWER 12

#define MyWire Wire

# comment to test codeberg push

byte diodePins[] = {13, 14, 26, 27};

struct sensor {
	uint16_t diodes;
	uint8_t id;
	char name[16];
	uint8_t nSensors;
	char unit[3][8];
	float value[3];
	uint8_t i2c_addr;
};


sensor mySensor;
OneWire my1Wire(PIN_1WIRE);
DallasTemperature myDS1820(&my1Wire);

uint16_t evalDiodes() {
	uint16_t rv = 0;
	// diode starting from 0
	pinMode(diodePins[0], OUTPUT);
	digitalWrite(diodePins[0], HIGH);
	for (byte i = 1; i < 4; i++) {
		pinMode(diodePins[i], INPUT_PULLDOWN);
		if (digitalRead(diodePins[i])){
			rv = i;
		}
	}
	// diode starting from 1
	pinMode(diodePins[0], INPUT_PULLDOWN);
	pinMode(diodePins[1], OUTPUT);
	digitalWrite(diodePins[1], HIGH);
	if (digitalRead(diodePins[0]))
		rv += 40;
	if (digitalRead(diodePins[2]))
		rv += 20;
	if (digitalRead(diodePins[3]))
		rv += 30;
	// diode starting from 2
	pinMode(diodePins[1], INPUT_PULLDOWN);
	pinMode(diodePins[2], OUTPUT);
	digitalWrite(diodePins[2], HIGH);
	if (digitalRead(diodePins[0])) 
		rv += 400;
	if (digitalRead(diodePins[1])) 
		rv += 100;
	if (digitalRead(diodePins[3])) 
		rv += 300;
	// diode starting from 3
	pinMode(diodePins[2], INPUT_PULLDOWN);
	pinMode(diodePins[3], OUTPUT);
	digitalWrite(diodePins[3], HIGH);
	if (digitalRead(diodePins[0]))
		rv += 4000;
	if (digitalRead(diodePins[1]))
		rv += 1000;
	if (digitalRead(diodePins[2]))
		rv += 2000;
	for (byte i = 0; i < 4; i++) {
		pinMode(diodePins[i], OUTPUT);
		digitalWrite(diodePins[i], LOW);
	}
	return rv;	
}

void scanI2C(sensor *sen) {
	uint8_t nDevices, error, address;
	nDevices = 0;

	MyWire.begin(I2C_SDA, I2C_SCL);

	for(address = 1; address < 127; address++ ) {
    	MyWire.beginTransmission(address);
    	error = MyWire.endTransmission();
    	//Serial.print(address);
    	if (error == 0) {
      		Serial.print(" I2C device found at address 0x");
      		if (address<16) {
        		Serial.print("0");
      		}
      		Serial.println(address,HEX);
      		nDevices++;
    	}
    	else if (error==4) {
      		Serial.print("Unknow error at address 0x");
      		if (address<16) {
        		Serial.print("0");
      		}
      		Serial.println(address,HEX);
    	}
    	//else
    	//	Serial.println(" nothing.");
    }
	if (nDevices == 0) {
		Serial.println("No I2C devices found\n");    
	}
	Serial.print(nDevices);
	Serial.println(" I2C devices found.");
  	sen->i2c_addr = address;
}

void initDS1820(sensor *sen) {
	myDS1820.begin();
	strcpy(sen->name, "DS1820");
	strcpy(sen->unit[0], "°C");
	sen->nSensors = 1;
}

void setup() {
	Serial.begin(115200);
	Serial.println(" *** START ***");
	pinMode(SENSOR_POWER, OUTPUT);
}

void loop() {
	Serial.println(evalDiodes());
	delay(1000);
	mySensor.diodes = evalDiodes();
	switch (mySensor.diodes) {
		case 301:
			Serial.println("I2C-Sensor!");
			digitalWrite(SENSOR_POWER, HIGH);
			scanI2C(&mySensor);
			break;
		case 2001:
			Serial.println("DS1820!");
			mySensor.diodes = 2001;
			digitalWrite(SENSOR_POWER, HIGH);
			initDS1820(&mySensor);
			Serial.print(mySensor.name);
			Serial.print(" ");
			myDS1820.requestTemperatures(); 
  			mySensor.value[0] = myDS1820.getTempCByIndex(0);
  			Serial.print(mySensor.value[0]);
  			Serial.print(" ");
			Serial.println(mySensor.unit[0]);
			break;
		case 340:
			Serial.println("Purely analog");
			mySensor.diodes = 340;
			break;
		default:
			Serial.print("Unknown diode combi ");
			Serial.println(mySensor.diodes);
			break;
	}
	delay(2000);

}
