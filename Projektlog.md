 
# UDL
## Universal Data Logger

## Features

- log to uSD-card or MQTT or both
- display 
- automatic identification of sensor/input
- extendable (more sensor definitions on SD card)
- sensor ID by diodes connected to 4 IOs
- RTC
- 18650 powered

## Pre-Tests
- I2C sensors work using I2C bus on two analog inputs (202103)
- DS1820 works on same pins (202103)
- display works (eSPI library div. examples) (202103) and after building into 
  housing (202103)
- uSD works (on same SPI as display) with uSD-SD-adapter as card reader (202103, 
  SD-Test example) and after building into housing (202103)
- identification of diodes - works (202103) but only if diodes do not somehow get into a chain - 
  idea for a config file: 0AK0 or A00K to identify where the diode is located. This provides 12+1
  possibilities
- idea for a mounting-post with clips tested. - works (202103)

## Cabling
Periphery | Colour | ESP32 pin
--- | --- | ---
RTC C | grey | 22
RTC D | purple | 21
LCD LED | dark green | 17?
LCD SCK | green | 18
LCD MOSI | yellow | 23
LCD DC | purple | 2
LCD Reset | grey | 4
LCD CS | white | 15
SD SCK | green (= LCD SCK) | 18
SD MISO | blue | 19
SD MOSI | yellow (= LCD MOSI) | 23
SD CS | green | 5
ground switch SD & LCD | orange | 16
RJ45 | white | 33 (SDA)
RJ45 | brown | powerGPIO 12
RJ45 | yellow | 13
RJ45 | green | GND
RJ45 | red | 32 (SCL)
RJ45 | black | 14
RJ45 | orange | 26
RJ45 | blue | 27
Joystick common | white | GND
Joystick up | brown | 34
Joystick | green | 35
Joystick | yellow | 36
Joystick | grey | 39
Joystick center | rose | 25

## Sources of Information
1. https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
2. http://www.esp32learning.com/code/esp32-and-ds3231-rtc-example.php
3. https://www.xtronical.com/esp32ili9341/
4. http://www.interfacebus.com/Secure_Digital_Card_Pinout.html
5. https://randomnerdtutorials.com/esp32-i2c-communication-arduino-ide/#3
6. https://iotdesignpro.com/projects/logging-temperature-and-humidity-data-on-sd-card-using-esp32
7. https://github.com/arduino-libraries/SD
8. https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf
9. https://github.com/marian-craciunescu/ESP32Ping/issues
 
